import mysql.connector
from mysql.connector import Error

create_table_products = """CREATE TABLE tbl_products (ProductID int(11) NOT NULL, Product_Name VARCHAR(70) NOT NULL, Price FLOAT NOT NULL, Quantity TINYINT NOT NULL, PRIMARY KEY (ProductID))"""
insert_data_products = """INSERT INTO tbl_products (ProductID, Product_Name, Price, Quantity) VALUES """
create_table_buyers = """CREATE TABLE tbl_buyers (BuyerID int(11) NOT NULL, Buyer_Name VARCHAR(70) NOT NULL, Product_Bought_ID int(11) NOT NULL, Quantity TINYINT NOT NULL, PRIMARY KEY (BuyerID), FOREIGN KEY(Product_Bought_ID) REFERENCES tbl_products(ProductID))"""
insert_data_buyers = """INSERT INTO tbl_buyers (BuyerID, Buyer_Name, Product_Bought_ID, Quantity) VALUES """

def connect_to_database():
    try:
        global con
        con = mysql.connector.connect (host = 'localhost', database = 'testdb', user = 'root', password = 'bablubabalu')
    except Error as e:
        print('Connection problem: ', e)

def showAll(tbl_type):
    try:
        connect_to_database()
        selectTable = """SELECT * FROM """ + tbl_type
        cursor = con.cursor()
        cursor.execute(selectTable)
        for i in cursor:
            print(i)
    except Error as e:
        print('Failed to get table: ', e)
    finally:
        if con.is_connected():
            cursor.close()
            con.close()

def check(tbl_type, ID):
    try:
        connect_to_database()
        if tbl_type == 'tbl_products':
            check_sql = "select * from " + tbl_type + " WHERE ProductID = " + ID
            cursor = con.cursor()
            cursor.execute(check_sql)
            lines = cursor.fetchall()

            for line in lines:
                print("ID:", line[0], "\nProduct:", line[1], "\nPrice:", line[2], "\nQuantity:", line[3])
        
        elif tbl_type == 'tbl_buyers':
            check_sql = "select * from " + tbl_type + " WHERE BuyerID = " + ID
            cursor = con.cursor()
            cursor.execute(check_sql)
            lines = cursor.fetchall()

            for line in lines:
                print("ID:", line[0], "\nBuyer:", line[1], "\nProduct:", line[2], "\nQuantity:", line[3])
    except Error as e:
        print('Fail to check: ' + e)
    finally:
        if con.is_connected():
            cursor.close()
            con.close()

def create_tb(create_table):
    try:
        connect_to_database()
        cursor = con.cursor()
        cursor.execute(create_table)
        print('Table created successfully')
    except Error as e:
        print('Failed to create table: ', e)
    finally:
        if con.is_connected():
            cursor.close()
            con.close()

def insert_dt():
    try:
        connect_to_database()
        print('Initializing data insertion...')
        table = input('Which table you wanna modify? \'1\' for tbl_products, \'2\' for tbl_buyers\n')
        quantity = input('Type how many datas you want to insert: ')
        if quantity == '0':
            pass
        else:
            if table == '1':
                insert_data = insert_data_products
                print('Inserting products data...')
                for i in range(int(quantity)):    
                    prodID = input("Product ID: ")
                    prodName = input("Product name: ")
                    prodPrice = input("Product price: ")
                    prodQuant = input("Product quantity: ")

                    data = '(' + prodID + ',\'' + prodName + '\',' + prodPrice + ',' + prodQuant + ')'
                    if i == int(quantity) - 1:
                        insert_data += data
                    else:
                        insert_data += data + ', '

            elif table == '2':
                insert_data = insert_data_buyers
                print('Inserting buyers data...')
                for i in range(int(quantity)):    
                    buyerID = input("Buyer ID: ")
                    buyerName = input("Buyer name: ")
                    prodBought = input("Product bought: ")
                    prodQuant = input("Product quantity: ")

                    data = '(' + buyerID + ',\'' + buyerName + '\',' + prodBought + ',' + prodQuant + ')'
                    if i == int(quantity) - 1:
                        insert_data += data
                    else:
                        insert_data += data + ', '

            cursor = con.cursor()
            cursor.execute(insert_data)
            con.commit()
            print(cursor.rowcount, 'data(s) inserted successfully')
            if table == '1':
                showAll('tbl_products')
            elif table == '2':
                showAll('tbl_buyers')

    except Error as e:
        print('Failed to insert data(s): ', e)
    finally:
        if con.is_connected():
            if quantity != '0':
                cursor.close()
            con.close()

def update():
    try:
        connect_to_database()
        print('Initializing data update... ')
        tableType = input ('Which table you wanna update? \'1\' for tbl_products, \'2\' for tbl_buyers\n')
        if tableType == '1':
            showAll('tbl_products')
            connect_to_database()
            prodID = input('Type ProductID: ')
            check('tbl_products', prodID)
            connect_to_database()
            dataType = input('Type the data type you want to change (q for Quantity or p for Price): ')
            if dataType == 'p':
                print('Update products price in database')
                print('\nInsert new product price: ')
                prodPrice = input('Product price: ')
                declaration = """UPDATE tbl_products    
                SET Price = """ + prodPrice + """ WHERE ProductID = """ + prodID
            elif dataType == 'q':
                print('Update products quantity in database')
                print('\nInsert new product quantity: ')
                prodQuant = input('Product quantity: ')
                declaration = """UPDATE tbl_products    
                SET Quantity = """ + prodQuant + """ WHERE ProductID = """ + prodID
        elif tableType == '2':
            showAll('tbl_buyers')
            connect_to_database()
            buyerID = input('Type BuyerID: ')
            check('tbl_buyers', buyerID)
            connect_to_database()
            dataType = input('Type the data type you want to change (q for Quantity or p for Product): ')
            if dataType == 'p':
                print('Update products bought in database')
                print('\nInsert new product bought: ')
                buyerProd = input('Product bought: ')
                declaration = """UPDATE tbl_buyers    
                SET Product_Bought_ID = """ + buyerProd + """ WHERE BuyerID = """ + buyerID
            elif dataType == 'q':
                print('Update products quantity in database')
                print('\nInsert new product quantity: ')
                prodQuant = input('Product quantity: ')
                declaration = """UPDATE tbl_buyers    
                SET Quantity = """ + prodQuant + """ WHERE BuyerID = """ + buyerID
        cursor = con.cursor()
        cursor.execute(declaration)
        con.commit()
        print("Data updated successfully")
        verify = input("\nWish check the update? y = yes, n = no: ")
        if verify == 'y' and tableType == '1':
            check('tbl_products', prodID)
        elif verify == 'y' and tableType == '2':
            check('tbl_buyers', buyerID)
        else:
            pass

    except Error as e:
        print('Failed to update:', e)
    finally:
        if con.is_connected():
            cursor.close()
            con.close()

def delete():
    try:
        connect_to_database()
        print('Initializing data deletion...')
        tableType = input('Which table you wanna modify? \'1\' for tbl_products, \'2\' for tbl_buyers\n')
        if tableType == '1':
            showAll('tbl_products')
            connect_to_database()
            prodID = input('Type the ID from the product which you wanna delete: ')
            delfunc = """DELETE FROM tbl_products WHERE ProductID = """ + prodID
        elif tableType == '2':
            showAll('tbl_buyers')
            connect_to_database()
            buyerID = input('Type the ID from the buyer which you wanna delete: ')
            delfunc = """DELETE FROM tbl_buyers WHERE BuyerID = """ + buyerID
        cursor = con.cursor()
        cursor.execute(delfunc)
        con.commit()
        print('Data removed succesfully')
        verify = input("\nWish check the update? y = yes, n = no: ")
        if verify == 'y' and tableType == '1':
            showAll('tbl_products')
        elif verify == 'y' and tableType == '2':
            showAll('tbl_buyers')
    except Error as e:
        print('Failed to delete data: ', e)
    finally:
        if con.is_connected():
            cursor.close()
            con.close()

if __name__ == '__main__':
    create_tb(create_table_products)
    insert_dt()
    update()
    delete()
    create_tb(create_table_buyers)
    insert_dt()
    update()
    delete()

    print("Goodbye!")